﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GWHealth.Models
{
    public class GWEnvirnment
    {
        [Key]
        public string Name { get; set; }
        public string Masterid { get; set; }
        public string Description { get; set; }

        public string Start { get; set; }
        public string End { get; set; }
        public string Branch { get; set; }

        public string Owner1 { get; set; }
        public string Owner2 { get; set; }
        public string Owner3 { get; set; }    
        public string Type { get; set; }
        public string Schedule { get; set; }

        public string ModuleName { get; set; }

        public bool IsActive { get; set; }
        
        public bool IsApp { get; set; }

        public bool IsClusterd { get; set; }
        public bool IsDev { get; set; }
        public String BuildDate { get; set; }
        public String DeployDate { get; set; }
        public String CurrentLog { get; set; }
        public List<GWMachine> GWMachines { get; set; }
    }


    public class GWMachine
    {
        [Key]
        public string Name { get; set; }
        public string Description { get; set; }
       
        public string IPAddress { get; set; }
        public string Privateurl { get; set; }
        public string PublicUrl { get; set; }
        public string DatabaseServer { get; set; }
        public string BulildDate { get; set; }

        public string DeploydDate { get; set; }       

        public bool isBatch { get; set; }




    }

    
    public class GWModule
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string InfraOwner { get; set; }
        public string DevOwner { get; set; }
        public string QAOwner { get; set; }
        public string Purpose { get; set; }
                
        public GWModule()
        {

        }
    }


    public class GWDataFile
    {
        [Key]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public string Module { get; set; }
        public string Size { get; set; }
        public string Envirnment { get; set; }
    }
     



}
