﻿using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GWHealth.Models
{
    public class LoginViewModel
    {
        #region Properties  

        /// <summary>  
        /// Gets or sets to username address.  
        /// </summary>  
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Display(Name = "DisplayName")]
        public string DisplayName { get; set; }

        [Display(Name = "IsAuthenticated")]
        public bool IsAuthenticated { get; set; }


        /// <summary>  
        /// Gets or sets to password address.  
        /// </summary>  
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        public string RequestId { get; set; }
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);


        public GWEnvirnment ActiveEnvirnment { get; set; }
        public List<GWEnvirnment> GWEnvirnments { get; set; }

        public List<GWDataFile> GWDataFiles()
        {

            string path = "D:\\GWUploads";

            List<GWDataFile> files = new List<GWDataFile>();


            // Get a list of all subdirectories  
            var pfiles = from file in Directory.EnumerateFiles(path) select file;



            foreach (var f in pfiles)
            {
                string fileName = f;

                files.Add(new GWDataFile { Name = f, Path = f, Size = f.Length.ToString(), Description = f, Envirnment = "", Module = "" });

            }


            return files;

        }



        public void SetActiveEnvirnment(string Name)
        {

            foreach (var item in this.GWEnvirnments)
            {
                if (item.Name.ToLower() == Name.ToLower())
                {
                    ActiveEnvirnment = item;
                    break;
                }
            }


        }


        public GWEnvirnment GetQAEnvirnment(string Name)
        {
            string search = Name.Replace("Dev", "QA");

            foreach (var item in this.GWEnvirnments)
            {
                if (item.Name.ToLower() == search.ToLower())
                {
                    return item;
                }
            }

            return null;
        }

        public List<GWModule> GWModules { get; set; }

        public LoginViewModel()
        {
            GWEnvirnments = new List<GWEnvirnment>();

            GWModules = new List<GWModule>();


            //Read Master Json.

            var myJsonString = File.ReadAllText("Data\\MasterEnv.json");
            JArray jsonArray = JArray.Parse(myJsonString);

            var myJsonMachines = File.ReadAllText("Data\\Env.json");
            var jsonArray2 = JArray.Parse(myJsonMachines);

            foreach (JToken item in jsonArray)
            {
                string id = item["Masterid"].ToString();
                GWModules.Add(new GWModule { Id = id, Name = item["Name"].ToString(), Description = item["Description"].ToString(), Purpose = item["Purpose"].ToString(), DevOwner = item["Name"].ToString(), InfraOwner = item["Name"].ToString(), QAOwner = item["Name"].ToString() });

            }





            //var filteredObjs = jsonArray2.Where(x => x["Masterid"].Value<string>() == id);

            foreach (JToken jenv in jsonArray2)
            {

                GWEnvirnment env = new GWEnvirnment
                {
                    Name = jenv["Name"].ToString(),
                    Branch = jenv["Branch"].ToString(),
                    BuildDate = jenv["Build"].ToString(),
                    DeployDate = jenv["Deployed"].ToString(),
                    Schedule = jenv["MachineSchedule"].ToString(),
                    Owner1 = jenv["Owner1"].ToString(),
                    Owner2 = jenv["Owner2"].ToString(),
                    Owner3 = jenv["Owner3"].ToString(),
                    IsActive = (jenv["Active"].ToString() == "True") ? true : false,
                    IsClusterd = (jenv["isClusterd"].ToString() == "True") ? true : false,
                    Start = jenv["Start"].ToString(),
                    End = jenv["End"].ToString(),
                    Masterid = jenv["Masterid"].ToString(),
                    Type = jenv["Type"].ToString(),
                    IsDev = (jenv["isDev"].ToString() == "True") ? true : false,
                    IsApp = (jenv["isApp"].ToString() == "True") ? true : false,
                    ModuleName = getModuleName(GWModules, jenv["Masterid"].ToString()),
                    Description = jenv["Description"].ToString()
                };


                env.GWMachines = new List<GWMachine>();
                foreach (var machine in jenv["Machines"])
                {

                    env.GWMachines.Add(new GWMachine
                    {
                        Name = machine["Name"].ToString(),
                        IPAddress = machine["IP"].ToString(),
                        BulildDate = machine["BulildDate"].ToString(),
                        DatabaseServer = machine["DatabaseServer"].ToString(),
                        DeploydDate = machine["DeploydDate"].ToString(),
                        Description = "",                        
                        isBatch = (machine["isBatch"].ToString() == "True") ? true : false,                        
                        Privateurl = machine["Privateurl"].ToString(),
                        PublicUrl = machine["PublicUrl"].ToString()

                    });
                }

                GWEnvirnments.Add(env);
            }




        }

        private string getModuleName(List<GWModule> gWModules, string masterid)
        {
            foreach (var item in gWModules)
            {
                if (item.Id == masterid)
                {
                    return item.Name;
                }
            }
            return "";
        }




        #endregion
    }
}
