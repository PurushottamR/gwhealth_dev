﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace GWHealth.Controllers
{

   
    public class FileUploadController : Controller
    {

        static string path = "D:\\GWUploads";
        public FileUploadController()
        {

        }



        [HttpPost]
        public IActionResult Index(IFormFile[] files)
        {           

            // Iterate through uploaded files array
            foreach (var file in files)
            {
                // Extract file name from whatever was posted by browser
                var fileName = path + "\\"  + System.IO.Path.GetFileName(file.FileName);
                
                // If file with same name exists delete it
                if (System.IO.File.Exists( fileName))
                {
                    System.IO.File.Delete(fileName);
                }

                // Create new local file and copy contents of uploaded file
                using (var localFile = System.IO.File.OpenWrite(fileName))
                using (var uploadedFile = file.OpenReadStream())
                {
                    uploadedFile.CopyTo( localFile);
                }
            }
            if (Request.Form["Env"].ToString() != "")
            {             
                
                return RedirectToAction("Dataload", new RouteValueDictionary(new { controller = "Home", action = "Dataload", Id = Request.Form["Env"].ToString() , caller ="Upload" }  ));
            }
            else
            {
                return RedirectToAction("Index", "Home" );

            }           
            
        }
    }
}